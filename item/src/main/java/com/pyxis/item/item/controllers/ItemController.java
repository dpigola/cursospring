package com.pyxis.item.item.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.pyxis.item.item.models.Item;
import com.pyxis.item.item.models.Product;
import com.pyxis.item.item.models.service.ItemService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope//Actualiza componentes
@RestController
public class ItemController {

    private static Logger log = LoggerFactory.getLogger(ItemController.class);
    
    @Autowired
    @Qualifier("serviceRestTemp")
    private ItemService itemService;

    @GetMapping("/items")
    public List<Item> getItems(){
        return itemService.findAll();
    }

    @HystrixCommand(fallbackMethod = "errorTask")
    @GetMapping("/item/{id}/qty/{qty}")
    public Item getItem(@PathVariable Long id, @PathVariable Integer qty){
        return itemService.findById(id, qty);

    }

    

    public Item errorTask(Long id, Integer qty){
        Item item = new Item();
        Product prd = new Product();
        item.setStock(qty);
        prd.setId(id);
        prd.setPrice(500.00);
        prd.setName("Default Product");
        item.setProduct(prd);
        return item;

    }

    @Value("${configuration.text}")
    private String textMessage;

    @GetMapping("/configuration")
    public ResponseEntity<?> getConfig(@Value("${server.port}")String port){
        Map<String, String> json = new HashMap();
        json.put("Configuration", this.textMessage);
        json.put("Port", port);
        
        json.entrySet().forEach(value -> log.info(value.getKey()+ " "+ value.getValue()));        
        return new ResponseEntity<Map<String, String>>(json, HttpStatus.OK);

    } 

    @PostMapping("/item")
    @ResponseStatus(HttpStatus.CREATED)
    public Product create(@RequestBody Product prd){
        log.info(prd.getName()+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
        return itemService.save(prd);
    }

    @PutMapping("/item")
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    public Product update(@RequestBody Product prd){
        log.info(prd.getName()+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!2");
        return itemService.save(prd);
    }

    @DeleteMapping("/item/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public void delete(@PathVariable Long id){
        itemService.delete(id);
    }
}
