package com.pyxis.item.item.models.service;

import java.util.List;
import java.util.stream.Collectors;

import com.pyxis.item.item.client.ProductRestClient;
import com.pyxis.item.item.models.Item;
import com.pyxis.item.item.models.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("serviceFeign")
public class ItemServiceFeign implements ItemService{

    @Autowired
    private ProductRestClient feignClient;

    @Override
    public List<Item> findAll() {
        List<Product> lstProducts =  feignClient.findAll();
        return lstProducts.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());

    }

    @Override
    public Item findById(Long id, Integer qty) {
        return new Item(feignClient.findById(id), qty);
    }

    @Override
    public Product save(Product prd) {
        return feignClient.createProduct(prd);
    }

    @Override
    public void delete(Long id) {
        feignClient.deleteProduct(id);
        
    }
    
}
