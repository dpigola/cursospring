package com.pyxis.item.item.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.pyxis.item.item.models.Item;
import com.pyxis.item.item.models.Product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service("serviceRestTemp")
public class ItemServiceImpl implements ItemService {

    private static Logger log = LoggerFactory.getLogger(ItemServiceImpl.class);

    @Autowired
    private RestTemplate restClient;

    @Override
    public List<Item> findAll() {
        //List<Product> products = Arrays.asList(restClient.getForObject("http://localhost:8001/products", Product[].class));
        List<Product> products = Arrays.asList(restClient.getForObject("http://servicio-productos/products", Product[].class));
        return products.stream().map(p -> new Item(p, 1)).collect(Collectors.toList());

    }

    @Override
    public Item findById(Long id, Integer qty) {
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id",Long.toString(id));
        //Product prd = restClient.getForObject("http://localhost:8001/product/{id}",Product.class, pathVariables);
        Product prd = restClient.getForObject("http://servicio-productos/product/{id}",Product.class, pathVariables);
        return new Item(prd, qty);
    }

    @Override
    public Product save(Product prd) {
        HttpEntity<Product> body = new HttpEntity<Product>(prd);
        ResponseEntity<Product> response;
        log.info(body.getBody().getName()+"!!!!!!!!!!!!!1");
        if(body.getBody().getId() == 0)
            response = restClient.exchange("http://servicio-productos/product", HttpMethod.POST, body, Product.class);
        else
            response = restClient.exchange("http://servicio-productos/product", HttpMethod.PUT, body, Product.class);
        return response.getBody();
    }

    @Override
    public void delete(Long id) {
        Map<String, String> pathVaMap = new HashMap<String, String>();
        pathVaMap.put("id", id.toString());
        restClient.delete("http://servicio-productos/product/{id}", pathVaMap);
        
    }
    
}
