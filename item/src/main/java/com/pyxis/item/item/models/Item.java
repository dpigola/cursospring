package com.pyxis.item.item.models;

public class Item {
    private Product product;
    private int stock;

    public Item() {
    }

    public Item(Product product, int stock) {
        this.product = product;
        this.stock = stock;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Double getTotal(){
        return product.getPrice() * this.stock;

    }
    
}
