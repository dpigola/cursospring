package com.pyxis.item.item.models.service;

import java.util.List;

import com.pyxis.item.item.models.Item;
import com.pyxis.item.item.models.Product;


public interface ItemService {

    List<Item> findAll();

    Item findById(Long id, Integer qty);

    Product save(Product prd);

    public void delete(Long id);
}
