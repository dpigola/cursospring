package com.pyxis.item.item.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

import com.pyxis.item.item.models.Product;


@FeignClient(name = "servicio-productos")
public interface ProductRestClient {
    
    @GetMapping("/products")
    List<Product> findAll();

    @GetMapping("/product/{id}")
    Product findById(@PathVariable Long id);

    @PostMapping("/product")
    @PutMapping("/product")
    Product createProduct(@RequestBody Product prd);

    @DeleteMapping("/product/{id}")
    public void deleteProduct(@PathVariable Long id);

    

}
