package com.formacionbdi.springboot.app.products.springbootservicioproducts.models.service;

import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.Product;

import java.util.List;

public interface IProductService {

    public List<Product> findAll();
    public Product findById(Long id);


}
