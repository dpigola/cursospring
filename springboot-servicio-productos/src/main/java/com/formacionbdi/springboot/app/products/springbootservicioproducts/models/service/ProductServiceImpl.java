package com.formacionbdi.springboot.app.products.springbootservicioproducts.models.service;

import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.Product;
import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service //Con esta anotacion marco esta clase como un bean de spring
public class ProductServiceImpl implements IProductService{

    @Autowired//Inyeccion del servicio
    private ProductRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Product> findAll() {
        return (List<Product>)repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Product findById(Long id) {
        return  repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public Product save(Product prd) {
        return repository.save(prd);
    }
}
