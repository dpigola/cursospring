package com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.repository;

import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository <Product, Long>{
}
