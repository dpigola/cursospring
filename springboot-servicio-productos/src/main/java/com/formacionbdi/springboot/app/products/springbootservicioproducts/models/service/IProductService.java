package com.formacionbdi.springboot.app.products.springbootservicioproducts.models.service;

import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.Product;

import java.util.List;

public interface IProductService {

    List<Product> findAll();
    Product findById(Long id);
    void deleteById(Long id);
    public Product save(Product prd);


}
