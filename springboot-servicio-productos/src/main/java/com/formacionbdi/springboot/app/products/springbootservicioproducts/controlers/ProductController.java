package com.formacionbdi.springboot.app.products.springbootservicioproducts.controlers;

import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.entity.Product;
import com.formacionbdi.springboot.app.products.springbootservicioproducts.models.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController//Convierte todas las respuestas en json
public class ProductController {

    @Autowired
    private IProductService productService;

    @Autowired
    private Environment env;

    @Value("${server.port}")
    private Integer port;



    @GetMapping("/products")
    public List<Product> ListProducts(){
        return productService.findAll().stream().map(prd -> {
          //prd.setPort(Integer.parseInt(env.getProperty("local.server.port")));
            prd.setPort(port);
          return prd;
        }).collect(Collectors.toList());
    }

    @GetMapping("/product/{id}")
    public Product getProductDetail(@PathVariable Long id) throws Exception {
        Product prd = productService.findById(id);
        //prd.setPort(Integer.parseInt(env.getProperty("local.server.port")));
        prd.setPort(port);
        //if (1 < 2)
        //    throw new Exception("Lamentablemente no se pudo cargar el producto");
        /*try{
            Thread.sleep(2000L);
        }catch (Exception e){
            throw new Exception("TIMEOUTBEBE!");
        }*/
        return prd;
    }

    @PostMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    public Product createProduct(@RequestBody Product prd){
        return productService.save(prd);
    }

    @PutMapping("/product")
    @ResponseStatus(HttpStatus.OK)
    public Product updateProduct(@RequestBody Product prd){
        return productService.save(prd);
    }

    @DeleteMapping("/product/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteProduct(@PathVariable Long id){
        productService.deleteById(id);
    }
}
