package com.pyxis.zuul.zuulsever.filters;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TimeFilterPre extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(TimeFilterPre.class);

    @Override
    public Object run() throws ZuulException {

        
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(String.format("%s request enrutado a %s", request.getMethod(), request.getRequestURL().toString()));
        Long init = System.currentTimeMillis();
        request.setAttribute("InitialTime", init);
        return true;
    }

    @Override
    public boolean shouldFilter() {
        // Si devuelve true este metodo se ejecuta el filtrado, si lo quiero mas pillo
        // agrego logica, aca va a devolver true para probar
        return true;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public String filterType() {
        // Opciones pre, post y route para ver en que momento evaluar
        return "pre";
    }

}
