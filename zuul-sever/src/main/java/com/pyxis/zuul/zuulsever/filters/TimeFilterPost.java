package com.pyxis.zuul.zuulsever.filters;

import javax.servlet.http.HttpServletRequest;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TimeFilterPost extends ZuulFilter{

    private static Logger log = LoggerFactory.getLogger(TimeFilterPost.class);

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        log.info("POST FILTER!");

        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        
        Long init = (Long)request.getAttribute("InitialTime");
        Long end = System.currentTimeMillis();

        Long elapsedTime = end - init;

        log.info(String.format("Tiempo transcurrido: %s seg", elapsedTime.doubleValue()/1000.00));

        log.info(String.format("Tiempo transcurrido: %s ms", elapsedTime));
        
        return true;
    }

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        // TODO Auto-generated method stub
        return 0;
    }
    
}



/*


*/